﻿/* Program Id : Inclass 1
 * 
 * Purpose: Create a Simple Calculator
 * 
 * Created by: Sandeep Kaur, 15 January, 2019
 * 
 */using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    // Class initializig and declaring the string values
    public partial class Form1 : Form
    {
        string x = string.Empty;
        string y = string.Empty;
        string result;
        char operation;

        public Form1()
        {
            InitializeComponent();
        }


        private void Form1_Load(object sender, EventArgs e)
        {
            btnOne.Click += new EventHandler(btn_Click);
            btnTwo.Click += new EventHandler(btn_Click);
            btnThree.Click += new EventHandler(btn_Click);
            btnFour.Click += new EventHandler(btn_Click);
            btnFive.Click += new EventHandler(btn_Click);
            btnSix.Click += new EventHandler(btn_Click);
            btnSeven.Click += new EventHandler(btn_Click);
            btnEight.Click += new EventHandler(btn_Click);
            btnNine.Click += new EventHandler(btn_Click);
            btnZero.Click += new EventHandler(btn_Click);
            btnDot.Click += new EventHandler(btn_Click);
        }
        //this will show the value entered in the text box
        void btn_Click(object sender, EventArgs e)
        {
            try
            {
                Button btn = sender as Button;

                switch (btn.Name)
                {
                    case "btnOne":
                        txtInputValue.Text += "1";
                        break;
                    case "btnTwo":
                        txtInputValue.Text += "2";
                        break;
                    case "btnThree":
                        txtInputValue.Text += "3";
                        break;
                    case "btnFour":
                        txtInputValue.Text += "4";
                        break;
                    case "btnFive":
                        txtInputValue.Text += "5";
                        break;
                    case "btnSix":
                        txtInputValue.Text += "6";
                        break;
                    case "btnSeven":
                        txtInputValue.Text += "7";
                        break;
                    case "btnEight":
                        txtInputValue.Text += "8";
                        break;
                    case "btnNine":
                        txtInputValue.Text += "9";
                        break;
                    case "btnZero":
                        txtInputValue.Text += "0";
                        break;
                    case "btnDot":
                        if (!txtInputValue.Text.Contains("."))
                            txtInputValue.Text += ".";
                        break;

                }
            }
            // shows error for invalid entry by the user
            catch (Exception ex)
            {
                MessageBox.Show("Unexpected error is found. Details: " + ex.Message);
            }
        }
        private void txtInput_KeyPress(object sender, KeyPressEventArgs e)
        {
            switch (e.KeyChar)
            {
                case '1':
                case '2':
                case '3':
                case '4':
                case '5':
                case '6':
                case '7':
                case '8':
                case '9':
                case '0':
                
                break;
                default:
                    e.Handled = true;
                    MessageBox.Show("Only numbers, +, -, ., *, / are allowed");
                    break;
            }
        }

        private void btnPlus_Click(object sender, EventArgs e)
        {
            x = txtInputValue.Text;
            operation = '+';
            txtInputValue.Text = string.Empty;
        }

        private void btnMinus_Click(object sender, EventArgs e)
        {
            x = txtInputValue.Text;
            operation = '-';
            txtInputValue.Text = string.Empty;
        }

        private void btnDivide_Click(object sender, EventArgs e)
        {
            x = txtInputValue.Text;
            operation = '/';
            txtInputValue.Text = string.Empty;
        }

        private void btnMultiply_Click(object sender, EventArgs e)
        {
            x = txtInputValue.Text;
            operation = '*';
            txtInputValue.Text = string.Empty;
        }

        private void btnEqualTo_Click(object sender, EventArgs e)
        {
            y = txtInputValue.Text;

            double opr1, opr2;
            double.TryParse(x, out opr1);
            double.TryParse(y, out opr2);

            switch (operation)
            {
                case '+':
                    result = (opr1 + " " + "+" + " " + opr2 + " " + "=" + " " + (opr1 + opr2)).ToString();
                    break;

                case '-':
                    result = (opr1 + " " + "-" + " " + opr2 + " " + "=" + " " + (opr1 - opr2)).ToString();
                    break;

                case '*':
                    result = (opr1 + " " + "*" + " " + opr2 + " " + "=" + " " + (opr1 * opr2)).ToString();
                    break;

                case '/':
                    if (opr2 != 0)
                    {
                        result = (opr1 + " " + "/" + " " + opr2 + " " + "=" + " " + (opr1 / opr2)).ToString();
                    }
                    else
                    {
                        MessageBox.Show("Division by 0 not possible!!");
                    }
                    break;
            }

            txtInputValue.Text = result.ToString();
        }

        // buttton clear 

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtInputValue.Text = string.Empty;
            x = string.Empty;
            y = string.Empty;
        }
        
        //clear everything
        private void btnClearAll_Click(object sender, EventArgs e)
        {
             txtInputValue.Clear();
        }
    }
    }

